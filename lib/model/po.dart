import 'dart:async';

class PO {
  final String ponum;
  final String poDescription;
  final int poid;
  final double poTotalCost;
  final String reqDate;
  final String requestBy;
  final String vendorName;
  final int priority;
  final String currencycode;
  const PO({this.ponum,this.poDescription,this.poid,this.priority,this.currencycode,this.poTotalCost,this.reqDate,this.requestBy,this.vendorName});
}

abstract class PORepository {
  Future<List<PO>> fetch();
}

