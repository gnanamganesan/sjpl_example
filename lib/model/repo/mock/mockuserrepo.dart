import 'dart:async';
import '../../User.dart';

class MockLoginRepository implements LoginRepository{
  @override
  Future<List<Login>> login() {
    return new Future.value(logindetails);
  }
}
// Initializing username and password with mock details.
var logindetails =  <Login>[new Login(username:"booshan",password:"123")];