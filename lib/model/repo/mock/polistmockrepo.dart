import 'dart:async';

import '../../po.dart';


class PoListMockRepo implements PORepository {
  @override
  Future<List<PO>> fetch() {
    // TODO: implement fetch
    return new Future.value(mockPO);
  }
  int removePO(String ponum){
    int res=0;
    for(int i=0;i<mockPO.length;i++){
      final PO po =mockPO[i];
      if(po.ponum==ponum){
        mockPO.removeAt(i);
        res=1;
        print('Inside Remove:'+i.toString());
        print('Inside Res:'+res.toString());
      }
    }
    return res;
  }
}

var mockPO =  <PO>[
  new PO(poDescription: "Electrical Supplies",
  ponum: "500210",
  poid: 1560,
  priority: 1,
  poTotalCost: 50054.20,
  vendorName: "Caterpillar Pvt Ltd.,",
  reqDate: "27/09/2018",
  requestBy: "John Wilson",
  currencycode:"USD"
  ),
  new PO(poDescription: "Pump - 100 GPM",
  ponum: "500211",
  poid: 1560,
  priority: 2,
  poTotalCost: 49875.25,
  vendorName: "Helwig Pvt Ltd.,",
  reqDate: "12/09/2018",
  requestBy: "Rolan Smith",
  currencycode:"USD"
  ),
  new PO(poDescription: "Plumbing Supplies",
  ponum: "500212",
  poid: 1560,
  priority: 3,
  poTotalCost: 50054.20,
  vendorName: "Garden City Ltd.,",
  reqDate: "08/09/2018",
  requestBy: "James Wilson",
  currencycode:"USD"
  ),
];