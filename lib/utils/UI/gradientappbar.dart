import 'package:flutter/material.dart';
class GradientAppBar extends StatelessWidget {

  final String title;
  final double barHeight = 66.0;
  

  GradientAppBar(this.title);



  @override
  Widget build(BuildContext context) {
    final double statusBarHeight = MediaQuery
      .of(context)
      .padding
      .top;
    
    return new Container(
      padding: new EdgeInsets.only(top: statusBarHeight),
      height: statusBarHeight + barHeight,
      child: new Container(
        child: new Row(
          children: <Widget>[
            new IconButton(
              icon: new Icon(Icons.menu),
              tooltip: 'Navigation Menu',
              onPressed: null,
              disabledColor: Colors.white,
              alignment: Alignment.centerLeft,
            ),
            new Text(title,
            style: const TextStyle(
              color: Colors.white,
              fontFamily: 'Roboto',
              fontWeight: FontWeight.w500,
              fontSize: 20.0
              
            ),
            ),
            new IconButton(
              icon: new Icon(Icons.search),
              tooltip: 'Search',
              onPressed: null,
              disabledColor: Colors.white,
              alignment: Alignment.centerRight,
              
            ),
            new IconButton(
              icon: new Icon(Icons.sort),
              tooltip: 'Sort',
              onPressed: null,
              disabledColor: Colors.white,
              alignment: Alignment.centerRight,
            )
          ],
        ),
      ),
    
      decoration: new BoxDecoration(
        gradient: new LinearGradient(
          colors: [
            const Color.fromRGBO(123,102,177,1.0),
            const Color.fromRGBO(74,199,255, 1.0)
          ],
          begin: const FractionalOffset(0.0, 0.0),
          end: const FractionalOffset(1.0, 0.0),
          stops: [0.0, 1.0],
          tileMode: TileMode.clamp
        ),
      ),
    );
    
  }
}