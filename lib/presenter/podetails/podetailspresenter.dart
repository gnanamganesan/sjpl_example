import '../../di/di.dart';
import '../../model/podetails.dart';

abstract class POLineContract{
  void onLoadPOLineComplete(List<POLine>items);
  void onLoadPOLineError();  
}
/// Create a class with two instances
class POLinePresenter{
  POLineContract _view;
  POLineRepository _repository;

  POLinePresenter(this._view){
    /// Assigning repository with the instance of loginRepository from Injector method.    
    _repository = new Injector().poLineRepo;
  }

  void loadLineItems(){
    /// Fecthing and passing values to the repository.
    _repository
    .fetchPOLineItems()
    .then((d)=>_view.onLoadPOLineComplete(d))
    .catchError((onError)=> _view.onLoadPOLineError());
  }
}
