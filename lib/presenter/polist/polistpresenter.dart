import '../../di/di.dart';
import '../../model/po.dart';


abstract class POListViewContract {
  void onLoadPOListComplete(List<PO> poListItems);
  void onLoadPOListError();
}

class POListViewPresenter {
  POListViewContract _view;
  PORepository _repository;

  POListViewPresenter(this._view){
    _repository= new Injector().poRepo;
  }
  void loadPOList(){
    assert(_view!=null);
    _repository.fetch()
                .then((polist) => _view.onLoadPOListComplete(polist))
                .catchError((onError){
                  print("Error on fetch List:"+onError);
                  _view.onLoadPOListError();
                });
  }
}