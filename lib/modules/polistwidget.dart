import 'package:flutter/material.dart';
import '../model/po.dart';
import '../presenter/polist/polistpresenter.dart';
import 'podetailwidget.dart';


class POListWidget extends StatelessWidget {
  @override
  Widget build(BuildContext context) {
    // TODO: implement build
    return new Scaffold(
      body: new Column(
        children: <Widget>[
           new POList(), 
            
        ],
      ) 
    );
  }
}

class POList extends StatefulWidget{
  POList({Key key}) : super(key:key);

  @override
  _POListState createState() => new _POListState();
  
}

class _POListState extends State<POList> implements POListViewContract {
  POListViewPresenter _poListViewPresenter;
  List<PO> _poModel =new List();
  bool _isLoading;
  
  _POListState(){
    _poListViewPresenter=new POListViewPresenter(this);
  }

  @override
  void initState(){
    super.initState();
    _isLoading=true;
    _poListViewPresenter.loadPOList();
  }


  @override
  Widget build(BuildContext context) {
    // TODO: implement build
    Widget widget;
    if (_isLoading){
      widget=new Center(
        child: new Padding(
          padding: const EdgeInsets.only(left: 16.0, right: 16.0),
          child: new CircularProgressIndicator(),
        )

      );
    }
    else{
      widget=new Container(
        child: new Column(
          crossAxisAlignment: CrossAxisAlignment.start,
          mainAxisSize:MainAxisSize.min,
          children: <Widget>[
            new Flexible(
              child: new ListView.builder(
                shrinkWrap: true,
                itemCount: _poModel.length,
                itemBuilder: (BuildContext context, int index){
                  final PO poData=_poModel[index];
                  return _getPOListItem(poData);
                
                },
              ),
            )
          ],
        ),

      );
    }
    return widget;
  }
  
  Widget _getPOListItem(PO poData){
    Widget poListItemWidget;

    TextStyle headTextStyle =new TextStyle(
      fontFamily: 'Roboto-Regular',
      fontWeight: FontWeight.w600,
      fontSize: 15.2,
      
    );
    TextStyle subTextStyle =new TextStyle(
      fontFamily: 'Roboto-Regular',
      fontWeight: FontWeight.w400,
      fontSize: 11.2,
      color: Colors.grey
    );
    TextStyle dateTextStyle =new TextStyle(
      fontFamily: 'Roboto-Regular',
      fontWeight: FontWeight.w400,
      fontSize: 10.0,
      color: Colors.grey

    );
    TextStyle amountTextStyle =new TextStyle(
      fontFamily: 'Roboto-Regular',
      fontWeight: FontWeight.w400,
      fontSize: 21.0,
      color: Colors.blueAccent
    );
    
    TextStyle personTextStyle = new TextStyle(
      fontFamily: 'Roboto-Regular',
      fontWeight: FontWeight.w400,
      fontSize: 12.3,
      color: Colors.black

    );

    /// According different priority the priority flag in the card varies
    /// For Priority is 1 then Flag will be Red
    /// For Priority is 2 then Flag will be Blue
    /// For Other Priority then Flag will be Grey
    /// Priority Condition Checking
    String priorityIcon;
    if (poData.priority==1){
      priorityIcon='asset/highpriority_icon.png';
    }
    else if(poData.priority==2){
      priorityIcon='asset/mediumpriority_icon.png';
    }
    else{
      priorityIcon='asset/lowpriority_icon.png';
    }
    String totalCost;
    if (poData.currencycode == "USD") {
         totalCost = r'$ '+poData.poTotalCost.toString();
    } else if (poData.currencycode == "INR") {
         totalCost = "₹ "+poData.poTotalCost.toString();
    }
    poListItemWidget = new Padding(
      padding: const EdgeInsets.all(1.0),
        child: new GestureDetector(
          onTap: (){Navigator.push(context, new MaterialPageRoute(builder: (context)=> new PODetail(poData)));},
          child:new Card(
          child: new Container(
            padding:const EdgeInsets.only(top: 8.0,left:14.0,bottom: 12.7,right: 12.3),
            child: new Row(             
              children:[
                new Expanded(
                  child: new Column(
                    crossAxisAlignment: CrossAxisAlignment.start,
                    mainAxisSize: MainAxisSize.min,
                    children: [
                      new Container(
                        alignment: Alignment.centerLeft,
                        child: new Text(
                          poData.poDescription,
                          style: headTextStyle,
                          textAlign: TextAlign.left,
                          softWrap: true,
                        ),
                        
                      ),
                      new Container(
                        alignment: Alignment.centerLeft,
                        margin: const EdgeInsets.only(top: 8.3,bottom: 26.0) ,
                        child: new Text(
                          poData.vendorName,
                          style: subTextStyle,
                          textAlign: TextAlign.left,
                          softWrap: true,
                        ),
                        
                      ),
                      new Container(
                        alignment: Alignment.centerLeft,
                        padding: const EdgeInsets.only(left:2.0),
                        child:new Row(
                          children: [
                            new Image.asset('asset/person_icon.png'),
                            new Padding(
                              padding: new EdgeInsets.only(left: 3.0),
                            child:new Text(
                            poData.requestBy,
                            style: personTextStyle,
                            textAlign: TextAlign.left,
                            softWrap: true,
                        )
                            )

                          ],
                        )                       
                      ),
                    ],
                  ),
                ),
                new Expanded(
                  child: new Column(
                    mainAxisSize: MainAxisSize.min,
                    crossAxisAlignment: CrossAxisAlignment.end,
                    children: [
                      new Container(
                        alignment: Alignment.topRight,
                        margin: const EdgeInsets.only(left: 12.3,bottom: 16.3) ,
                        child: new Text(
                          poData.reqDate,
                          style: dateTextStyle,
                          textAlign: TextAlign.left,
                          softWrap: true,
                        ),   
                      ),
                      new Container(
                        alignment: Alignment.centerRight,
                        margin: const EdgeInsets.only(bottom: 10.0),
                        child: new Text(
                          totalCost,
                          style: amountTextStyle,
                          textAlign: TextAlign.right,
                          softWrap: true,
                        ),   
                      ),
                      new Container(
                        alignment: Alignment.centerRight,
                        margin: const EdgeInsets.only(left:95.0,top:6.0),
                        child:new Row(
                          children: [
                            new Image.asset(priorityIcon),
                            new Padding(
                              padding: new EdgeInsets.only(left:3.0),
                            child:new Text(
                            poData.ponum,
                            style: personTextStyle,
                            textAlign: TextAlign.right,
                            softWrap: true,
                        ),
                        )
                          ],
                        )   
                      )
                    ],
                  ),
                )
              ]
            )
          ),
        ),
        ),
    );

    return poListItemWidget;
  }
  

  @override
  void onLoadPOListComplete(List<PO> poListItems) {
    // TODO: implement onLoadPOListComplete
    setState((){
      _poModel=poListItems;
      _isLoading=false;
    });
  }

  @override
  void onLoadPOListError() {
    // TODO: implement onLoadPOListError
    Scaffold.of(context).showSnackBar(new SnackBar(
      content: new Text("Something went wrong while loading list..")
    )
    ); 
  }

}



