import 'package:flutter/material.dart';
import 'polistwidget.dart';


class MaxHome extends StatefulWidget{
  MaxHomePage createState() => new MaxHomePage();

}

class MaxHomePage extends State<MaxHome>{

  int _selectedDrawerIndex = 0;
  static String appBarTitle="Purchase Order";

  _getDrawerItemWidget(int pos) {
    switch (pos) {
      case 0:
        appBarTitle="Purchase Order";
        return new POListWidget();
      default:
        appBarTitle="Purchase Order";
        return new POListWidget();
    }
  }
  
  _onSelectItem(int index) {
    setState(() => _selectedDrawerIndex = index);
    Navigator.of(context).pop(); // close the drawer
}

  @override
  Widget build(BuildContext context) {
    // TODO: implement build
     TextStyle headTextStyle =new TextStyle(
      fontFamily: 'Roboto-Regular',
      fontWeight: FontWeight.w700,
      fontSize: 15.7,
      color: Colors.white
    );
    TextStyle listTextStyle = new TextStyle(
                fontFamily: 'Roboto-Regular',
                fontWeight: FontWeight.w500,
                fontSize: 15.2,
                color: Colors.white
              );
    var headerBackGround=new BoxDecoration(
        color: Colors.blueAccent,
        image: new DecorationImage(
          image: new AssetImage('asset/drawerbg.png'),
          fit:BoxFit.fill
        )
      );
    var headerContent= new Center(
      child: new Container(
        padding: EdgeInsets.only(top: 10.0),
        child: new Column(
        children:[ 
          new CircleAvatar(
          backgroundColor: Colors.white,
          child: new Image.asset('asset/logo.png'),
          radius: 33.0,
        ),
        new Padding(
          padding: const EdgeInsets.only(top: 15.0),
        child:new Text("Welcome Booshan !",style:headTextStyle))
        ]
        ),
      ),
    );
   
    var header= new DrawerHeader(child:headerContent,decoration: headerBackGround);
    var drawer =new Drawer(
      child: new Container(
            padding: new EdgeInsets.all(0.0),
            color: new Color.fromRGBO(76,76,86, 1.0),
      child: new Column(
        children: <Widget>[
          header,
          new Column(
            children:[
            new ListTile(
              leading: new Image.asset('asset/po_icon.png'),
              title: new Text('Purchase Order',
              style:listTextStyle,
              ),
              onTap: () => _onSelectItem(0)
            ),
            new ListTile(
              leading: new Image.asset('asset/pr_icon.png'),
              title: new Text('Purchase Request',
              style:listTextStyle,
              ),
              onTap: () => _onSelectItem(0)
            ),
            new ListTile(
              leading: new Image.asset('asset/wo_icon.png'),
              title: new Text('Workorder',
              style:listTextStyle,
              ),
              onTap: () => _onSelectItem(0)
            ),
            new ListTile(
              leading: new Image.asset('asset/invoice_icon.png'),
              title: new Text('Invoice',
              style:listTextStyle,
              ),
              onTap: () => _onSelectItem(0)
            ),
            new ListTile(
              leading: new Image.asset('asset/contract_icon.png'),
              title: new Text('Contract',
              style:listTextStyle,
              ),
              onTap: () => _onSelectItem(0)
            ),
            new Divider(height:2.0,color: new Color.fromRGBO(97,97,106,1.0),),
            new ListTile(
              leading: new Icon(Icons.settings,color: Colors.white),
              title: new Text('Settings',
              style: listTextStyle
              ),
            ),
            new ListTile(
              leading: new Image.asset('asset/logout_icon.png'),
              title: new Text('Sign out',
              style: listTextStyle
              ),
            )
          ]),
          
        ],

      ),
    )
      );
    Widget home= new Scaffold(
      drawer: drawer,
      body: new Stack(
        children: <Widget>[
          new CustomScrollView(
        slivers: <Widget>[
          new SliverAppBar(
            pinned: true,
            title: new Text(appBarTitle),
            expandedHeight: 60.0, ///Standard App Height
            flexibleSpace: const FlexibleSpaceBar(
              //title: const Text('Purchase Order'),
              background: const _AppBarBackground(),
            ),
            actions: <Widget>[
               new IconButton(
                icon: new Icon(Icons.search),
                onPressed: (){
                },
                
              ),
               new IconButton(
                icon: new Icon(Icons.sort),
                onPressed: (){
                },
                
              ),
            ],
          ),
         
        ],
      ),
      new Padding(
        padding: new EdgeInsets.only(top: 80.0),
       child:_getDrawerItemWidget(_selectedDrawerIndex)
      )
        ],
        
      ) 
      
    );
    return home;

  }

}

class _AppBarBackground extends StatelessWidget{

  const _AppBarBackground({Key key}) :super(key:key);
  @override
  Widget build(BuildContext context) {
    // TODO: implement build
    return new Stack(
      children: [
    new Positioned(
      child: new Image.asset(
        'asset/appbar.png'
      ),

    ),
      ]
    );
  }

}
