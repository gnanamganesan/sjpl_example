import 'package:flutter/material.dart';
import '../../model/User.dart';
import '../../presenter/Login/loginpresenter.dart';
import '../homewidget.dart';

class LoginPage extends StatefulWidget {
  @override
  _LoginPageState createState() => new _LoginPageState();
}

class _LoginPageState extends State<LoginPage> implements LoginContract {
  final scaffoldKey = new GlobalKey<ScaffoldState>();
  final formKey = new GlobalKey<FormState>();
  List<Login> _loginModel =new List();
  BuildContext ctx;
  bool _isLoading = false;
  String lusername, lpassword;
  LoginPresenter _presenter;

  _LoginPageState() {
    _presenter = new LoginPresenter(this);
  }
  @override
  void initState(){
    super.initState();
    _isLoading=true;
  }
  void _submit() {
    final form = formKey.currentState;
    if (form.validate()) {
      print("Inside Form Validate");
      setState(() { 
        _isLoading = true;
        form.save();
        if (lusername=="booshan" && lpassword=="123"){
         print("Inside Validate");
           Navigator.push(context, new MaterialPageRoute(builder: (context)=> new MaxHome()));
       }else
       print("Wrong Credentials Entered");
        });
      //final Login loginData=_loginModel[1];
      //print("User:"+lusername);
    }
  }
   void _showSnackBar(String error) {
      scaffoldKey.currentState.showSnackBar(new SnackBar(
      content: new Text(error),
));
}

   @override
  Widget build(BuildContext context) {
    return new Scaffold(
      body: new Container(
        decoration: new BoxDecoration(
          image: new DecorationImage(
            image: new ExactAssetImage('asset/login_bg.jpg'),// Login  Background Image
            fit: BoxFit.fill,
          ),
        ),
        child: new Container(
            margin: const EdgeInsets.fromLTRB(10.0, 50.3, 30.0, 0.0),
            child: new ListView(
              padding: const EdgeInsets.all(0.0),
              children: <Widget>[
                new Stack(
                  alignment: AlignmentDirectional.bottomCenter,
                  children: <Widget>[
                    new Column(
                      mainAxisAlignment: MainAxisAlignment.center,
                      children: <Widget>[
                        new Container(
                          height: 180.0,
                          width: 200.0,
                          alignment: Alignment.center,
                          child:
                              new Image.asset('asset/logo.png', height: 120.0),
                        ),
                        new Container(
                            //  margin: new EdgeInsets.symmetric(horizontal: 2.0),
                            child: new Column(
                          mainAxisAlignment: MainAxisAlignment.center,
                          children: <Widget>[
                            new Form(
                                key: formKey,
                                child: new Theme(
                                    data: new ThemeData(
                                        brightness: Brightness.dark,
                                        primarySwatch: Colors.blueGrey,
                                        accentColor: Colors.blueAccent,
                                        inputDecorationTheme:
                                            new InputDecorationTheme(
                                                labelStyle: new TextStyle(
                                                    color:
                                                        const Color(0xFFd4d4d4),
                                                    fontSize: 16.2))),
                                    child: new Column(
                                      mainAxisAlignment:
                                          MainAxisAlignment.spaceAround,
                                      children: <Widget>[
                                        new Row(
                                          mainAxisSize: MainAxisSize.min,
                                          children: [
                                            new Padding(
                                              padding: const EdgeInsets.only(
                                                  left: 3.0),
                                            ),
                                            new Image.asset(
                                                'asset/user_icon.png'),
                                            new Padding(
                                              padding: const EdgeInsets.only(
                                                  left: 26.3, top: 0.0),
                                            ),
                                            new Expanded(
                                              child: new TextFormField(
                                                key: new Key('lusername'),
                                                decoration: new InputDecoration(
                                                    labelText: "Username"),
                                                validator: (val) =>
                                                    !val.isNotEmpty
                                                        ? 'Enter username'
                                                        : null,
                                                onSaved: (val) =>
                                                    lusername = val,
                                                keyboardType:
                                                    TextInputType.text,
                                                obscureText: false,
                                              ),
                                            ),
                                          ],
                                        ),
                                        new Row(
                                          mainAxisSize: MainAxisSize.min,
                                          children: [
                                            new Padding(
                                              padding: const EdgeInsets.only(
                                                  left: 3.0),
                                            ),
                                            new Image.asset(
                                                'asset/pass_icon.png'),
                                            new Padding(
                                              padding: const EdgeInsets.only(
                                                  left: 20.3, top: 0.0),
                                            ),
                                            new Expanded(
                                              child: new TextFormField(
                                                key: new Key('lpassword'),
                                                decoration: new InputDecoration(
                                                  labelText: "Password",
                                                ),
                                                validator: (val) =>
                                                    val.length <= 0
                                                        ? 'Invalid password.'
                                                        : null,
                                                onSaved: (val) =>
                                                    lpassword = val,
                                                keyboardType:
                                                    TextInputType.text,
                                                obscureText: true,
                                              ),
                                            ),
                                          ],
                                        ),
                                      ],
                                    )))
                          ],
                        )),
                        new Container(
                          margin: const EdgeInsets.only(
                              left: 100.0, right: 100.0, top: 20.0),
                          decoration: new BoxDecoration(
                            border: new Border(
                              top: new BorderSide(
                                  width: 2.0, color: const Color(0xff6f67a8)),
                              bottom: new BorderSide(
                                  width: 2.0, color: const Color(0xFF2b97b1)),
                              left: new BorderSide(
                                  width: 2.0, color: const Color(0xff6f67a8)),
                              right: new BorderSide(
                                  width: 2.0, color: const Color(0xFF2b97b1)),
                            ),
                          ),
                          child: new MaterialButton(
                            textColor: Colors.blue[400],
                            key: new Key('Login'),
                            child: new Text(
                              "Login",
                              style: new TextStyle(
                                  fontSize: 15.3, fontWeight: FontWeight.bold),
                            ),
                            onPressed: _submit,
                          ),
                        ),
                      ],
                    ),
                  ],
                )
              ],
            )),
      ),
    );
  }

  @override
  void onLoginError() {
    // TODO: implement onLoginError
    print("Error");
        setState(() {
          _isLoading = false;
    });
      }
    
      @override
      void onLoginSuccess(List<Login> login) {
        // TODO: implement onLoginSuccess
        setState(() {
          _isLoading = false;
        });
      }
    
      
    }
    
 
