import 'package:flutter/material.dart';

class MaxHome extends StatefulWidget{
  MaxHomePage createState() => new MaxHomePage();

}
class MaxHomePage extends State<MaxHome>  {
  final scaffoldkey  = new GlobalKey<ScaffoldState>();
  VoidCallback showBottomSheetCallBack;

    @override
    void initState(){
     super.initState();  
        
     showBottomSheetCallBack = showBottomSheet;
    }
    void showBottomSheet() {
      setState((){
        showBottomSheetCallBack = null;

      });
      scaffoldkey.currentState.showBottomSheet((context) {
        
        return new Container(
          height: 300.0,
          color: Colors.teal,
          child: new Center(
            child: new Text("Attachments"),
          ),
        );
      }).closed.whenComplete((){
        if (mounted){
          setState((){
            showBottomSheetCallBack = showBottomSheet;
          });
        }
      });
    }
  @override
  Widget build(BuildContext context) {
    // TODO: implement build
        Widget home= new Scaffold(
        key: scaffoldkey,
       body: new CustomScrollView(
        slivers: <Widget>[
          new SliverAppBar(
            pinned: true,
            expandedHeight: 60.0, 
            flexibleSpace: const FlexibleSpaceBar(              
              background: const _AppBarBackground(),
            ),
            actions: <Widget>[
             new Container(
          child: new Row(
            children:[
              new IconButton(
                padding: const EdgeInsets.only(left:16.7, right: 28.7),
                icon: new BackButtonIcon(),
                onPressed: null,
                disabledColor: Colors.white,
                alignment: Alignment.centerLeft,
              ),
              new Text(
                "PO - Detail",
                style: const TextStyle(
                color: Colors.white,
                fontFamily: 'Roboto',
                fontWeight: FontWeight.w500,
                fontSize: 19.0                
                ),
              ),
              new IconButton(
                padding: const EdgeInsets.only(right:27.0,left: 120.3),
                icon: new Icon(Icons.attach_file),
                onPressed: showBottomSheetCallBack,
                disabledColor: Colors.white,
                alignment: Alignment.centerRight,
              )
            ],
          ),
        ),
            ],
          )
        ],
      ),
    );
    return home;
  }

}
class _AppBarBackground extends StatelessWidget{

  const _AppBarBackground({Key key}) :super(key:key);
  @override
  Widget build(BuildContext context) {
    // TODO: implement build
    return new Stack(
      
      children: [
    new Positioned(
      child: new Image.asset(
        'asset/appbar.png',
        alignment: Alignment.center,
      ),

    ),
      ]
    );
  }

}