// This is a basic Flutter widget test.
// To perform an interaction with a widget in your test, use the WidgetTester utility that Flutter
// provides. For example, you can send tap and scroll gestures. You can also use WidgetTester to
// find child widgets in the widget tree, read text, and verify that the values of widget properties
// are correct.

import 'package:flutter/material.dart';
import 'package:flutter_test/flutter_test.dart';
import '../lib/modules/login/loginwidget.dart';


void main() {
 testWidgets('non-empty username and password, valid account, calls sign in, succeeds', (WidgetTester tester) async {
    // Build our app and trigger a frame.
     
     Widget testWidget = new MediaQuery(
      data: new MediaQueryData(),
      child: new MaterialApp(home: new LoginPage())
    );
      await tester.pumpWidget(testWidget);

    Finder userName = find.byKey(new Key('lusername'));
    print('Username found');
    await tester.enterText(userName, 'booshan');
    print('Username entered');
    Finder passwordField = find.byKey(new Key('lpassword'));
    print('Password found');
    await tester.enterText(passwordField, '123');
    Finder loginButton = find.byKey(new Key('Login'));
    await tester.tap(loginButton);

    await tester.pump();
    
    // Verify that our counter starts at 0.
    //expect(find.text('0'), findsOneWidget);
   // expect(find.text('1'), findsNothing);

    // Tap the '+' icon and trigger a frame.
   // await tester.tap(find.byIcon(Icons.add));
   // await tester.pump();

    // Verify that our counter has incremented.
    //expect(find.text('0'), findsNothing);
    //expect(find.text('1'), findsOneWidget);
  });
}
